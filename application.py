from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/quemsomosnos")
def quemsomosnos():
    return render_template("quemsomosnos.html")

@app.route("/baixe")
def baixe():
    return render_template("baixe.html")

@app.route("/colabore")
def colabore():
    return render_template("colabore.html")

@app.route("/nossahistoria")
def nossahistoria():
    return render_template("nossahistoria.html")

@app.route("/equipe")
def equipe():
    return render_template("equipe.html")

@app.route("/contato")
def contato():
    return render_template("contato.html")
